from __future__ import print_function
import numpy as np
np.random.seed(1234)

from keras.datasets import cifar10
from keras.layers import merge, Input, Dropout
from keras.layers.convolutional import Convolution2D, ZeroPadding2D, AveragePooling2D
from keras.layers.core import Dense, Activation, Flatten
from keras.layers.normalization import BatchNormalization
from keras.layers.pooling import MaxPooling2D
from keras.models import Model
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import np_utils
from keras.regularizers import l2, activity_l2, l1
from keras.optimizers import SGD, Adam, RMSprop, Nadam
from keras.callbacks import LearningRateScheduler, ModelCheckpoint, EarlyStopping
from keras.callbacks import History
import keras.backend as K
from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
import sys
import csv
from itertools import izip
sys.setrecursionlimit(10000)

def bnReluConv(depth, kernelx, kernely,stride=(1,1),weight_decay=1e-4):
    def f(net):
	net = BatchNormalization(mode=0,axis=1,gamma_regularizer=l2(0.0001),beta_regularizer=l2(0.0001))(net)
	net = Activation('relu')(net)
	net = Convolution2D(depth, kernelx, kernely, border_mode='same', init='he_normal', subsample=stride, bias=False,W_regularizer=l2(weight_decay))(net)
        return net
    return f

def bnReluConvDrop(depth, kernelx, kernely,droprate=0.,stride=(1,1),weight_decay=1e-4):
    def f(net):
	net = BatchNormalization(mode=0,axis=1,gamma_regularizer=l2(0.0001),beta_regularizer=l2(0.0001))(net)
	net = Activation('relu')(net)
	net = Convolution2D(depth, kernelx, kernely, border_mode='same', init='he_normal', subsample=stride, bias=False,W_regularizer=l2(weight_decay))(net)
	if droprate>0.:
		net=Dropout(droprate)(net)
        return net
    return f


##########################

def denseBlock_layout(net,feature_map_n_list,n_filter,droprate=0.,weight_decay=1e-4):
	n_convs=len(feature_map_n_list)
	layer_list=[net]
	n_filter = n_filter
	for i in xrange(len(feature_map_n_list)):
		net = bnReluConvDrop(feature_map_n_list[i], 3, 3,droprate=droprate,stride=(1,1),weight_decay=weight_decay)(net)
		layer_list.append(net)
		net      = merge(layer_list , mode='concat',concat_axis=1)
		n_filter+=feature_map_n_list[i]
	return net,n_filter
########################



def mPdrop(pool_size=(2,2), droprate = 0.0):
    def f(net):
	net = MaxPooling2D(pool_size, strides=None, border_mode='valid', dim_ordering='default')(net)
        if droprate != 0.:
            net = Dropout(droprate)(net)
        return net
    return f
def aPdrop(pool_size=(2,2), droprate = 0.0):
    def f(net):
	net = AveragePooling2D(pool_size, strides=None, border_mode='valid', dim_ordering='default')(net)
        if droprate != 0.:
            net = Dropout(droprate)(net)
        return net
    return f

def DropAp(pool_size=(2,2),strides=(2,2), droprate = 0.0):
    def f(net):
        if droprate != 0.:
            net = Dropout(droprate)(net)
        net = AveragePooling2D(pool_size, strides=strides, border_mode='valid', dim_ordering='default')(net)
        return net
    return f




# the CIFAR10 images are 32x32 RGB with 10 labels
img_rows, img_cols = 32, 32
img_channels = 3
nb_classes = 10
penalization = 0.0001

# the data, shuffled and split between train and test sets
(X_train, y_train), (X_test, y_test) = cifar10.load_data()

X_train, y_train = shuffle(X_train, y_train)
X_train, X_test, y_train, y_test = train_test_split(X_train, y_train, test_size=0.1)

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
#pixelMean = np.mean(X_train, axis=0)

img_dim = X_train.shape[1:]
if K.image_dim_ordering() == "th":
    n_channels = img_dim[0]
    for i in range(n_channels):
        mean = np.mean(X_train[:, i, :, :]) #using train only mean and std
        std = np.std(X_train[:, i, :, :])
        X_train[:, i, :, :] = (X_train[:, i, :, :] - mean) / std
        X_test[:, i, :, :] = (X_test[:, i, :, :] - mean) / std

elif K.image_dim_ordering() == "tf":
    n_channels = img_dim[-1]
    for i in range(n_channels):
        mean = np.mean(X_train[:, :, :, i]) #using train only mean and std
        std = np.std(X_train[:, :, :, i])
        X_train[:, :, :, i] = (X_train[:, :, :, i] - mean) / std
        X_test[:, :, :, i] = (X_test[:, :, :, i] - mean) / std


print('X_train shape:', X_train.shape)
print(X_train.shape[0], 'train samples')
print(X_test.shape[0], 'test samples')

# convert class vectors to binary class matrices
Y_train = np_utils.to_categorical(y_train, nb_classes)
Y_test = np_utils.to_categorical(y_test, nb_classes)


droprate=0.2
batch_size = 64
nb_epoch = 300
data_augmentation = True
n = 3  #number of dense blocks

k = 12 #growth rate: number of feature maps at each convolution output inside a dense block
depth = 40 #number of layers inside a dense block
assert (depth - 4) % 3 == 0, "Depth must be 3 N + 4"

# layers in each dense block
nb_layers = int((depth - 4) / 3)
k0= 16
n_filter = k0

feature_map_n_list = [k]*nb_layers



img_input = Input(shape=(img_channels, img_rows, img_cols))
x = Convolution2D(k0, 3, 3, border_mode='same', init='he_normal', W_regularizer = l2(0.0001),bias=False)(img_input)

for i in xrange(n-1):
    x,n_filter = denseBlock_layout(x,feature_map_n_list,n_filter,droprate=droprate)
    x = BatchNormalization(mode=0,axis=1,gamma_regularizer=l2(0.0001),beta_regularizer=l2(0.0001))(x)
    x = Activation('relu')(x)
    x = Convolution2D(n_filter, 1, 1, border_mode='same', init='he_normal', W_regularizer = l2(0.0001),activation='linear',bias=False)(x)
    x = DropAp(droprate=droprate)(x)
    #x = aPdrop(droprate=droprate)(x)

x,n_filter = denseBlock_layout(x,feature_map_n_list,n_filter,droprate=droprate)
#x = Convolution2D(n_filter, 1, 1, border_mode='same', init='he_normal', W_regularizer = l2(0.0001))(x)

x = BatchNormalization(mode=0,axis=1,gamma_regularizer=l2(0.0001),beta_regularizer=l2(0.0001))(x)
x = Activation('relu')(x)
x = AveragePooling2D((8, 8), strides=(1, 1))(x)

x = Flatten()(x)
#x = Dense(512,activation='relu')(x)
preds = Dense(nb_classes, activation='softmax', W_regularizer=l2(penalization))(x)

model = Model(input=img_input, output=preds)





#paper
def lr_schedule(epoch):
    if epoch < 150: rate = 0.1
    elif epoch < 225: rate = 0.01
    elif epoch < 300: rate = 0.001
    else: rate = 0.0001
    print (rate)
    return rate

model.summary()
#exit(1)
model.compile(SGD(lr=0.1, momentum=0.9, decay=0.0, nesterov=True), loss='categorical_crossentropy',
              metrics=['accuracy'])

lrate = LearningRateScheduler(lr_schedule)


filename  = "dense_net_cifar10"
filepath  = filename+".h5"
results   = filename+".results"
model_checkpoint = ModelCheckpoint(filepath, monitor='val_acc', verbose=1, save_best_only=True, save_weights_only=False, mode='auto')
early_stopping   = EarlyStopping(monitor='val_acc', patience=100, verbose=1, mode='auto')
#callbacks = [lrate,model_checkpoint,early_stopping]
callbacks = [lrate,model_checkpoint]


if not data_augmentation:
    print('Not using data augmentation.')
    history=model.fit(X_train, Y_train,
              batch_size=batch_size,
              nb_epoch=nb_epoch,
              validation_data=(X_test, Y_test),
              shuffle=True,
	      callbacks=callbacks)
else:
    print('Using real-time data augmentation.')

    # this will do preprocessing and realtime data augmentation
    datagen = ImageDataGenerator(
        featurewise_center=False,  # set input mean to 0 over the dataset
        samplewise_center=False,  # set each sample mean to 0
        featurewise_std_normalization=False,  # divide inputs by std of the dataset
        samplewise_std_normalization=False,  # divide each input by its std
        zca_whitening=False,  # apply ZCA whitening
        rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
        width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
        height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
        horizontal_flip=True,  # randomly flip images
        vertical_flip=False)  # randomly flip images

    # compute quantities required for featurewise normalization
    # (std, mean, and principal components if ZCA whitening is applied)
    datagen.fit(X_train)

    # fit the model on the batches generated by datagen.flow()
    history = model.fit_generator(datagen.flow(X_train, Y_train,
                                     batch_size=batch_size, shuffle=True),
                        samples_per_epoch=X_train.shape[0],
                        #nb_val_samples=X_test.shape[0],
                        nb_epoch=nb_epoch,
                        validation_data=(X_test, Y_test), callbacks=callbacks,)
f= open(results, 'wb')
writer = csv.writer(f)
writer.writerows(izip( history.history['acc'],history.history['val_acc'],history.history['loss'],history.history['val_loss']))
f.close()
